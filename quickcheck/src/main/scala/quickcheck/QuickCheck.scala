package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  def toList(h: H): List[Int] =
    if (isEmpty(h)) Nil
    else {
      findMin(h) :: toList(deleteMin(h))
    }

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  lazy val genHeap: Gen[H] = for {
    x <- arbitrary[A]
    h <- oneOf(value(empty), genHeap)
  } yield insert(x, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("min2") = forAll { (a: Int, b: Int) =>
    val h = insert(a, insert(b, empty))
    findMin(h) == (a min b)
  }

  property("insert and delete") = forAll { a: Int =>
    val h = insert(a, empty)
    isEmpty(deleteMin(h))
  }

  property("sorting") = forAll { a: H =>
    val l = toList(a)
    l == l.sorted
  }

  property("two lists") = forAll { (a: H, b: H) =>
    val minA = findMin(a)
    val minB = findMin(b)
    val m = meld(a, b)
    val minM = findMin(m)
    minM == (minA min minB)
  }

  property("del then ins") = forAll { a: H =>
    val minA = findMin(a)
    val b = insert(minA, deleteMin(a))
    toList(b) == toList(a)
  }
  
  property("mass insert") = forAll { l: List[Int] =>
    val h = l.foldRight(empty)((a, b) => insert(a, b))
    toList(h) == l.sorted
  }
  

}
