package simulations

import common._

class Wire {
  private var sigVal = false
  private var actions: List[Simulator#Action] = List()

  def getSignal: Boolean = sigVal

  def setSignal(s: Boolean) {
    if (s != sigVal) {
      sigVal = s
      actions.foreach(action => action())
    }
  }

  def addAction(a: Simulator#Action) {
    actions = a :: actions
    a()
  }

  override def toString = s"Wire($sigVal)"
}

abstract class CircuitSimulator extends Simulator {

  val InverterDelay: Int
  val AndGateDelay: Int
  val OrGateDelay: Int

  def probe(name: String, wire: Wire) {
    wire addAction {
      () => afterDelay(0) {
        println(
          "  " + currentTime + ": " + name + " -> " +  wire.getSignal)
      }
    }
  }

  def inverter(input: Wire, output: Wire) {
    def invertAction() {
      val inputSig = input.getSignal
      afterDelay(InverterDelay) { output.setSignal(!inputSig) }
    }
    input addAction invertAction
  }

  def andGate(a1: Wire, a2: Wire, output: Wire) {
    def andAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(AndGateDelay) { output.setSignal(a1Sig & a2Sig) }
    }
    a1 addAction andAction
    a2 addAction andAction
  }

  //
  // to complete with orGates and demux...
  //

  def orGate(a1: Wire, a2: Wire, output: Wire) {
    def orAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(OrGateDelay) { output.setSignal(a1Sig || a2Sig) }
    }
    a1 addAction orAction
    a2 addAction orAction
  }

  def orGate2(a1: Wire, a2: Wire, output: Wire) {
    val c, d, e = new Wire
    inverter(a1, c)
    inverter(a2, d)
    andGate(c, d, e)
    inverter(e, output)
  }

  def demux0(in: Wire, c: Wire, o1: Wire, o2: Wire) {
    val d = new Wire
    inverter(c, d)
    andGate(c, in, o1)
    andGate(d, in, o2)
  }

  def demux(in: Wire, c: List[Wire], out: List[Wire]) {
    // c = List(false,true) => out=List(false,false,in,false)
    if (out.size == 2) {
      demux0(in, c.head, out(0), out(1))
    } else {
      val ld: List[Wire] = out.sliding(2, 2).map {
        case o1 :: o2 :: Nil =>
          val d = new Wire
          demux0(d, c.last, o1, o2)
          d
        case x => throw new Error(s"Aha! ${out.sliding(2, 2).toList} $c")
      }.toList
      demux(in, c.init, ld)
    }
  }

}

object Circuit extends CircuitSimulator {
  val InverterDelay = 1
  val AndGateDelay  = 3
  val OrGateDelay   = 5

  def andGateExample {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    probe("in1", in1)
    probe("in2", in2)
    probe("out", out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    in1.setSignal(true)
    run

    in2.setSignal(true)
    run
  }

  def demuxExample {
    val in = new Wire
    val c = List(false, false, true, false).map{ x =>
      val v = new Wire
      v.setSignal(x)
      v
    }.toList
    val out = List(1 to 16).map{ x =>
      val v = new Wire
      v.setSignal(false)
      v
    }.toList

    in.setSignal(true)
    demux(in, c, out)
    run

    in.setSignal(true)
    run
  }

  //
  // to complete with orGateExample and demuxExample...
  //
}

object CircuitMain extends App {
  // You can write tests either here, or better in the test class CircuitSuite.
  Circuit.andGateExample
  Circuit.demuxExample
}
