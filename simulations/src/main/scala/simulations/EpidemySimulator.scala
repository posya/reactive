package simulations

import math.random
import scala.util.Random

class EpidemySimulator extends Simulator {

  def randomBelow(i: Int): Int = (random * i).toInt
  def isChance(x: Double): Boolean = random < x

  protected[simulations] object SimConfig {
    val population: Int     = 300
    val roomRows: Int       = 8
    val roomColumns: Int    = 8

    // DONE: additional parameters of simulation
    val prevalenceRate: Double        = 0.01
    val transRate: Double             = 0.4
    val deathRate: Double             = 0.25
  }

  import SimConfig._

  // DONE: construct list of persons

  
  //
  // Additional vars for debugging
  //
  
  protected[simulations] var wasInfected = 0
  protected[simulations] var wasDied = 0
  protected[simulations] var wasUnableToMove = 0
  protected[simulations] var wasTryingToMove = 0


  //
  // Persons construction
  //
  
  val persons: List[Person] = (1 to population).map { i=> val p = new Person(i); p }.toList
  persons.take((population * prevalenceRate).toInt).foreach{ _.infect() }

  def personsAt(row: Int, col: Int): List[Person] = persons.filter(p => p.row == row && p.col == col)
  def isCanMoveTo(row: Int, col: Int) = personsAt(row, col).forall(_.isNotVisiblyInfected)

  
  private var crisisCount = (1 / deathRate).toInt
  def crisis: Boolean = {
    crisisCount = crisisCount - 1
    if (crisisCount == 0) {
      crisisCount = (1 / deathRate).toInt
      true
    } else false
  }






  class Person (val id: Int) {

    override def toString = {
      var s = s"Person($id) ="
      s += s" ($row, $col)"
      if (infected) s += " infected"
      if (sick) s += " sick"
      if (immune) s += " immune"
      if (dead) s += " dead"
      s
    }

    var infected = false
    var sick = false
    var immune = false
    var dead = false

    def isCanBeInfected = !isInfectious
    def isInfectious = infected || sick || immune || dead
    def isVisiblyInfected = sick || dead
    def isNotVisiblyInfected = !isVisiblyInfected


    // demonstrates random number generation
    var row: Int = randomBelow(roomRows)
    var col: Int = randomBelow(roomColumns)

    //
    // DONE with simulation logic
    //

    def cleanRooms = {
      def nearRooms: Set[(Int, Int)] = List(
        ((row + 1 + roomRows) % roomRows, col),
        ((row - 1 + roomRows) % roomRows, col),
        (row, (col + 1 + roomColumns) % roomColumns),
        (row, (col - 1 + roomColumns) % roomColumns)
      ).toSet

      nearRooms.filter { case (x, y) => isCanMoveTo(x, y) }
    }

    def neighbors = personsAt(row, col)



    def assertDead() { val __DEBUG__ = false; if (__DEBUG__) assert(!dead, s"Day $currentDay, $this") }

    /**
     * infects person
     */
    def infect() {
      assertDead()
      if (isCanBeInfected){
        wasInfected += 1
        infected = true
        afterDelay(6) { assertDead(); sick = true }
        afterDelay(14) {
          if (crisis) {
            wasDied += 1
            dead = true
          } else {
            afterDelay(2) { assertDead(); sick = false; immune = true }
            afterDelay(4) { assertDead(); sick = false; immune = false; infected = false }
          }
        }
      }
    }

    def moveAction() {
      if (!dead) {
        planToMove()
        wasTryingToMove += 1
        val moveTo = Random.shuffle(cleanRooms).headOption
        moveTo match {
          case Some((x, y)) =>
            row = x; col = y
            if (neighbors.exists(_.isInfectious) && isChance(transRate)) infect()
          case None => { wasUnableToMove += 1 }
        }
      }
    }

    def planToMove() {
      if (!dead) afterDelay(randomBelow(5) + 1)(moveAction())
    }

    planToMove()

  }
}
