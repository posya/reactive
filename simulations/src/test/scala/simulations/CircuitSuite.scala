package simulations

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CircuitSuite extends CircuitSimulator with FunSuite {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5
  
  test("andGate example") {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "and 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === false, "and 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 3")
  }

  test("orGate") {
    val in1, in2, out = new Wire
    orGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    assert(out.getSignal === false, "or 1")

    in1.setSignal(true)
    run

    assert(out.getSignal === true, "or 2")

    in2.setSignal(true)
    run

    assert(out.getSignal === true, "or 3")
  }

  test("orGate2") {
    val in1, in2, out = new Wire
    orGate2(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    assert(out.getSignal === false, "or 1")

    in1.setSignal(true)
    run

    assert(out.getSignal === true, "or 2")

    in2.setSignal(true)
    run

    assert(out.getSignal === true, "or 3")
  }

  test("demux0") {
    // c = List(false,true) => out=List(false,false,in,false)

    val in, c, o1, o2 = new Wire

    demux0(in, c, o1, o2)
    in.setSignal(true)

    c.setSignal(true)
    run
    assert((o1.getSignal, o2.getSignal) === (true, false), "de 1")

    c.setSignal(false)
    run
    assert((o1.getSignal, o2.getSignal) === (false, true), "de 2")
  }

  test("demux1") {
    val in: Wire = new Wire
    val c: List[Wire] = List(false, true).map {x => val v = new Wire; v.setSignal(x); v}.toList
    val out: List[Wire] = (1 to 4).map {_ => val v = new Wire; v}.toList
    demux(in, c, out)

    in.setSignal(true)
    run
    assert(out.map(_.getSignal).toList === List(false, false, true ,false), "de 1")

    in.setSignal(false)
    run
    assert(out.map(_.getSignal).toList === List(false, false, false ,false), "de 2")
  }

  test("demux2") {
    val in = new Wire
    val out = (1 to 4).map {x => val v = new Wire; v.setSignal(false); v}.toList
    val c = List(false, false).map {x => val v = new Wire; v.setSignal(x); v}.toList
    demux(in, c, out)

    in.setSignal(true)
    run
    assert(out.map(_.getSignal).toList === List(false, false, false ,true), "de 1")

    in.setSignal(false)
    run
    assert(out.map(_.getSignal).toList === List(false, false, false ,false), "de 2")
  }

  test("demux more") {

    val in = new Wire
    val c = List(
      false, false,
      true, false
    ).map {x => val v = new Wire; v.setSignal(x); v}.toList

    val out = (1 to 16).map {x => val v = new Wire; v}.toList
    demux(in, c, out)

    in.setSignal(true)
    run
    assert(out.map(_.getSignal).toList === List(false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false), "de 1")

    in.setSignal(false)
    run
    assert(out.map(_.getSignal).toList === List(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false), "de 2")

  }


  //
  // to complete with tests for orGate, demux, ...
  //

}
