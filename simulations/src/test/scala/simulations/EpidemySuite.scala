package simulations

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class EpidemySuite extends FunSuite {

  test("prevalence rate"){
    val es = new EpidemySimulator
    val numInfected = es.persons.count(_.infected)

    assert(numInfected == es.SimConfig.population * es.SimConfig.prevalenceRate,
      "prevalence rate should be 0.01"
      )
  }

  test("dead person stays dead"){
    val es = new EpidemySimulator

    val chosenOne = es.persons.last
    chosenOne.infected = true
    chosenOne.sick = true
    chosenOne.dead = true
    chosenOne.immune = false

    val(row, col) = (chosenOne.row, chosenOne.col)

    val testDays = 100

    while(!es.agenda.isEmpty && es.agenda.head.time < testDays){
      es.next

      assert(chosenOne.dead, "Dead person should keep dead state")
      assert(chosenOne.infected, "Dead person keeps infected")
      assert(!chosenOne.immune, "Dead person cannot become immune")
      assert(chosenOne.sick, "Dead person keeps sick")
      assert(chosenOne.col == col && chosenOne.row == row, "Dead person cannot move")
    }
  }

  test("life cycle"){
    var personDied = true;
    while(!personDied){
      val es = new EpidemySimulator

      val incubationTime = 6
      val dieTime = 14
      val immuneTime = 16
      val healTime = 18

      val prevalenceRate = 0.01
      val transRate = 0.4
      val dieRate = 0.25

      val infectedPerson = es.persons.find { _.infected }.get

      //before incubation time
    	while(es.agenda.head.time < incubationTime){
    		assert(infectedPerson.infected, "Infected person keeps infected in 6 days")
    		assert(!infectedPerson.sick, "Infected person does not get sick in 6 days")
    		assert(!infectedPerson.immune, "Infected person cannot become immune in 6 days")
    		assert(!infectedPerson.dead, "Infected person does not die in 6 days")
    		es.next
    	}

      //incubation time has passed, there should be an event for getting sick
      assert(es.agenda.head.time == incubationTime, "You should set a 'sick' event after incubation time")
      while(es.agenda.head.time == incubationTime) es.next
      assert(infectedPerson.sick, "Infected person should become sick after 6 days")

      //wait for dieTime
      while(es.agenda.head.time < dieTime){
      	assert(infectedPerson.infected, "Sick person keeps infected")
      	assert(infectedPerson.sick, "Sick person keeps sick before turning immune")
      	assert(!infectedPerson.immune, "Sick person is not immune")
      	assert(!infectedPerson.dead, "Sick person does not die before 14 infected days")
      	es.next
      }

      assert(es.agenda.head.time == dieTime, "You should set a 'die' event (decides with a probability 25% whether the person dies) after 14 days")
      while(es.agenda.head.time == dieTime) es.next
    }
  }


  test("transmissibility rate"){
	  var infectedTimes = 0
	  for(i <- 0 to 100){
		  val es = new EpidemySimulator
		  val healthyPerson = (es.persons find {p => !p.infected}).get
		  es.persons.filter(p => p != healthyPerson) foreach {_.infected = true}

      while(es.agenda.head.time < 6) es.next

      infectedTimes = infectedTimes + (if(healthyPerson.infected) 1 else 0)
	  }
	  assert(infectedTimes > 0, "A person should get infected according to the transmissibility rate when he moves into a room with an infectious person")
  }


//  test("death rate"){
//    val testCount = 20
//	  var infected = 0
//    var died = 0
//    var died1 = 0
//    for(i <- 0 to testCount){
//
//		  val es = new EpidemySimulator
//      while(es.agenda.head.time < 150) es.next
//
//      died = died + es.persons.filter(_.dead).size
//      infected = infected + es.wasInfected
//	  }
//    val rate = died.toDouble * 100 / infected
//    val diff = 25 - rate
//
//	  assert(diff.abs < 0.2, s"diff = $diff, infected = $infected, died = $died")
//  }


  test("dies within 14 days") {
    val es = new EpidemySimulator
    val start = es.persons.filter ( p => p.infected )
    val mortalityRate = (start.length * es.SimConfig.deathRate).toInt

    while(es.agenda.head.time <= 14) es.next

    val died = start.filter (p => p.dead)
    val diff = mortalityRate - died.length
    assert(diff === 0, s"At ${es.agenda.head.time} day $mortalityRate of people should have died, but ${died.length} died. Have $diff left")
  }


  test("raw and col >= 0 and < 8"){
    def between(x: Int, a: Int, b: Int): Boolean = a <= x && x <= b
    val es = new EpidemySimulator
    while(es.agenda.head.time < 150) {
      val a = es.persons.filter{ p =>
        !between(p.row, 0, es.SimConfig.roomRows - 1) ||
        !between(p.col, 0, es.SimConfig.roomColumns - 1) }
      assert(a.length == 0, a)
      es.next
    }
  }


  test("persons moves for a 6 days"){
    val es = new EpidemySimulator
    val startPersonsPos = es.persons.map{ p => p.id -> (p.row, p.col) }.toMap
    var a: List[es.Person] = es.persons
    while(es.agenda.head.time < 6) {
      es.next
      a = a.filter{ p => (p.row, p.col) == startPersonsPos(p.id) }
    }
    assert(a.length == 0, s"$a \n $startPersonsPos \n Tried to move ${es.wasTryingToMove} \n Unable ${es.wasUnableToMove}")
  }


  test("infected persons moves for a 6 days"){
    val es = new EpidemySimulator
    es.persons.foreach(_.infect())
    val startPersonsPos = es.persons.map{ p => p.id -> (p.row, p.col) }.toMap
    var a: List[es.Person] = es.persons
    while(es.agenda.head.time < 6) {
      es.next
      a = a.filter{ p => (p.row, p.col) == startPersonsPos(p.id) }
    }
    assert(a.length == 0, s"$a \n $startPersonsPos \n Tried to move ${es.wasTryingToMove} \n Unable ${es.wasUnableToMove}")
  }


  test("infected persons dies in 14 days"){
    val es = new EpidemySimulator
    es.persons.foreach(_.infect())
    while(es.agenda.head.time <= 14) {
      es.next
    }
    assert(es.wasDied === es.SimConfig.deathRate * es.SimConfig.population , s"${es.currentDay}")
  }


  test("dies within 150 days") {
    def getRate = {
      val es = new EpidemySimulator

      while(es.agenda.head.time < 150) es.next

      val died = es.persons.count(p => p.dead)
      val rate = died.toDouble / es.SimConfig.population * 100
      val infected = es.wasInfected
      (rate, died, infected)
    }

    val rates = for (i <- 1 to 10) yield getRate
    val (rateSum, diedSum, infSum) = rates.foldLeft((0: Double, 0: Int, 0: Int)){case (sum, (r, d, i)) => (sum._1 + r, sum._2 + d, sum._3 + i)}
    val rate = rateSum / 10
    val died = diedSum.toDouble / 10
    val infected = infSum.toDouble / 10

    val out = s"In average of 10 times by 150 days, $died of people died, $infected was infected. Rate is $rate."

    //println(out)
    assert(condition = true, out)
  }


}
